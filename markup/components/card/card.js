const cardSlider = document.querySelectorAll('.n-card--offers');
[...cardSlider].forEach((i, e) => {
    const prev = i.querySelector('.n-card__slider-btn--prev');
    const next = i.querySelector('.n-card__slider-btn--next');
    const container = i.querySelector('.n-card__slider-container');
    if (prev && next && container) {
        const slider = new Swiper(container, {
            navigation: {
                nextEl: next,
                prevEl: prev
            }
        });
    }
});

