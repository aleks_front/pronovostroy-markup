import objectFitImages from 'object-fit-images';
// https://github.com/bfred-it/object-fit-images
objectFitImages();

import Stickyfill from 'stickyfilljs';
// https://github.com/wilddeer/stickyfill


let elements = document.querySelectorAll('.is-sticky');
Stickyfill.add(elements);


// import stickybits from 'stickybits';
// const isSticky = document.querySelectorAll('.is-sticky');
// [...isSticky].forEach((i, e) => {
//     stickybits('.is-sticky');
// });

